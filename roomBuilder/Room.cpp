//
// Created by moonchel on 26.11.2020.
//
#include "Room.h"
Room::Room(){};

void Room::printRoom() {
    for(int i=0;i<m_roomGun.size();i++){
        std::cout<<i<<"->"<<"'";
        std::cout<<m_roomGun.at(i)->getName();
        std::cout<<"' ";
    }
    for(int i=0;i<m_roomFood.size();i++){
        std::cout<<m_roomGun.size()+i<<"->"<<"'";
        std::cout<<m_roomFood.at(i)->getName();
        std::cout<<"' ";
    }
    std::cout<<"Note: furniture is unpickable.";
    for(int i=0;i<m_furniture.size();i++){
        std::cout<<m_roomFood.size()+m_roomGun.size()+i<<"->"<<"'";
        std::cout<<m_furniture.at(i)->getfName();
        std::cout<<"' ";
    }
    for(int i=0;i<m_roomMonst.size();i++){
        std::cout<<m_furniture.size()+m_roomFood.size()+m_roomGun.size()+i<<"->"<<"'";
        std::cout<<m_roomMonst.at(i)->getName();
        std::cout<<"' ";
    }
}
void Room::printText() {
    m_textState->printText();
}
void Room::addItem(Furniture* f){
    m_furniture.push_back(f);
};
void Room::addItem(Weapon* w){
    m_roomGun.push_back(w);
};
void Room::addItem(Food* g){
    m_roomFood.push_back(g);
};
void Room::addItem(Monsters* m){
    m_roomMonst.push_back(m);
};
Weapon* Room::getWeapon(int index){
    for(int i = 0;i<m_roomGun.size();i++){
        if(m_roomGun.at(index) == m_roomGun.at(i)){
            return m_roomGun.at(i);
        }
        else{
            cout<< "Not Found\n";
            return nullptr;
        }
    }
}
Food* Room::getFood(int index){
    for(int i = 0;i<m_roomFood.size();i++){
        if(m_roomFood.at(index) == m_roomFood.at(i)){
            return m_roomFood.at(i);
        }
        else{
            cout<< "Not Found\n";
            return nullptr;
        }
    }
}

