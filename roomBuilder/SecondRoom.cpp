//
// Created by moonchel on 14.12.2020.
//

#include "SecondRoom.h"
SecondRoom::SecondRoom():RoomBuilder(){
};

void SecondRoom::buildRoomFurniture(){
    Furniture* furniture = new Furniture("Box");
    Weapon* magnum = new Weapon(25,"Magnum");
    Food* apple = new Food("Apple");
    furniture->addItem(magnum);
    furniture->addItem(apple);
    m_room->addItem(furniture);
};
void SecondRoom::buildRoomGun(){
    m_room->addItem(new Weapon(19,"M16"));
};
void SecondRoom::buildRoomFood(){
    m_room->addItem(new Food("Nuka-Cola"));
};
void SecondRoom::buildRoomText(){
    m_room->m_textState = new Second();
};
void SecondRoom::buildRoomMonsters(){
    m_room->addItem(new Monsters(60,15,"Zombie"));
};