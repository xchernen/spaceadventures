//
// Created by moonchel on 09.12.2020.
//

#ifndef ROOMBUILDER_ROOMBUILDER_H
#define ROOMBUILDER_ROOMBUILDER_H
#include "Room.h"


class RoomBuilder:public Room {
protected:
Room* m_room;
public:
    RoomBuilder();
    void createNewRoom();
    virtual void buildRoomFurniture() = 0;
    virtual void buildRoomGun() = 0;
    virtual void buildRoomFood() = 0;
    virtual void buildRoomText() = 0;
    virtual void buildRoomMonsters() = 0;
    Room* getRoom();

};


#endif //ROOMBUILDER_ROOMBUILDER_H
