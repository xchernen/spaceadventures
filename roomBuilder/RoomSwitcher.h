//
// Created by Asus on 11.01.2021.
//

#ifndef SPACEADVENTURES_ROOMSWITCHER_H
#define SPACEADVENTURES_ROOMSWITCHER_H
#include "FirstRoom.h"
#include "SecondRoom.h"

class RoomSwitcher: public FirstRoom, public SecondRoom{
    int input = 0;

public:
    void Switch(Room *r1, Room *r2);
    RoomSwitcher();

};


#endif //SPACEADVENTURES_ROOMSWITCHER_H
