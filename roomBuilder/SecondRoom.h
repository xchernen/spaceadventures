//
// Created by moonchel on 14.12.2020.
//

#ifndef SPACEADVENTURES_SECONDROOM_H
#define SPACEADVENTURES_SECONDROOM_H
#include "RoomBuilder.h"
#include "../TextState/Second.h"
#include "../Monsters.h"
#include "Room.h"

class SecondRoom: public RoomBuilder {
public:
    SecondRoom();
    void buildRoomFurniture();
    void buildRoomGun();
    void buildRoomFood();
    void buildRoomText();
    void buildRoomMonsters();
};


#endif //SPACEADVENTURES_SECONDROOM_H
