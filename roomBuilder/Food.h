//
// Created by moonchel on 29.11.2020.
//

#ifndef SPACEADVENTURES_FOOD_H
#define SPACEADVENTURES_FOOD_H
#include <iostream>

class Food{
    std::string m_name;
public:
    Food(std::string name);
    std::string getName();
};


#endif //SPACEADVENTURES_FOOD_H
