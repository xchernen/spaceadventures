//
// Created by Asus on 21.11.2020.
//

#include "weapon.h"

Weapon::Weapon(int bonusAttack, std::string itemName){
    m_name = itemName;
    m_bonusAttack = bonusAttack;
}

std::string Weapon::getName(){
    return m_name;
}