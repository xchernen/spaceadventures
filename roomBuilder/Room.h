//
// Created by moonchel on 26.11.2020.
//

#ifndef ROOMBUILDER_A_H
#define ROOMBUILDER_A_H
#include "Furniture.h"
#include <vector>
#include <iostream>
#include "../TextState/First.h"
#include "../Monsters.h"

class Room {
    std::vector <Furniture*> m_furniture;
    std::vector <Weapon* > m_roomGun;
    std::vector <Food* > m_roomFood;
    std::vector <Monsters* > m_roomMonst;
    //bool m_isAjzekinRoom;
public:
    Room();
    void printText();
    void printRoom();
    void addItem(Furniture* f);
    void addItem(Weapon* w);
    void addItem(Food* g);
    void addItem(Monsters* m);
    Weapon* getWeapon(int index);
    Food* getFood(int index);
    bool getIsAjzekinRoom();
    TextState* m_textState;
};


#endif //ROOMBUILDER_A_H
