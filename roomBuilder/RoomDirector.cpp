//
// Created by moonchel on 09.12.2020.
//

#include "RoomDirector.h"


RoomDirector::RoomDirector(RoomBuilder* builder){
    m_builder = builder;
};
void RoomDirector::setRoomBuilder(RoomBuilder* builder){
    m_builder = builder;
};
Room* RoomDirector::createRoom(){
    m_builder->buildRoomFurniture();
    m_builder->buildRoomFood();
    m_builder->buildRoomGun();
    m_builder->buildRoomText();
    m_builder->buildRoomMonsters();
    return m_builder->getRoom();
};