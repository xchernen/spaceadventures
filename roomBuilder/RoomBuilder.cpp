    //
// Created by moonchel on 09.12.2020.
//

#include "RoomBuilder.h"

RoomBuilder::RoomBuilder(){
    m_room = new Room();
};

void RoomBuilder::createNewRoom(){
    m_room = new Room();
};

Room* RoomBuilder::getRoom(){
    return m_room;
};
