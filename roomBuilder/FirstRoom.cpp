//
// Created by moonchel on 09.12.2020.
//

#include "FirstRoom.h"

FirstRoom::FirstRoom():RoomBuilder() {

}

void FirstRoom::buildRoomFurniture(){
    Furniture* furniture = new Furniture("Box");
    Weapon* magnum = new Weapon(20,"Magnum");
    Food* apple = new Food("Apple");
    furniture->addItem(magnum);
    furniture->addItem(apple);
    m_room->addItem(furniture);
};

void FirstRoom::buildRoomGun(){
    m_room->addItem(new Weapon(20,"Shotgun"));
};
void FirstRoom::buildRoomFood(){
    m_room->addItem(new Food("Tomato Soup"));
};
void FirstRoom::buildRoomText() {
    m_room->m_textState = new First();
}
void FirstRoom::buildRoomMonsters(){
    Monsters* ogre = new Monsters(40,10,"Ogre magi");
    m_room->addItem(ogre);
};
