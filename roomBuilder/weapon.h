//
// Created by Asus on 21.11.2020.
//

#ifndef SPACEADVENTURES_WEAPON_H
#define SPACEADVENTURES_WEAPON_H
#include <iostream>
using std::string;

class Weapon{
protected:
    string m_name;
    float m_bonusAttack;
public:
    Weapon(int bonusAttack,std::string itemName);
    std::string getName();
};



#endif //SPACEADVENTURES_WEAPON_H
