//
// Created by moonchel on 09.12.2020.
//

#ifndef ROOMBUILDER_FIRSTROOM_H
#define ROOMBUILDER_FIRSTROOM_H
#include "RoomBuilder.h"
#include "../TextState/First.h"
#include "../Monsters.h"

using std::cout;
using std::endl;
using std::cin;
using std::string;

class FirstRoom: public RoomBuilder {
public:
    FirstRoom();
    void buildRoomFurniture();
    void buildRoomGun();
    void buildRoomFood();
    void buildRoomText();
    void buildRoomMonsters();
};


#endif //ROOMBUILDER_FIRSTROOM_H
