//
// Created by moonchel on 09.12.2020.
//

#ifndef ROOMBUILDER_ROOMDIRECTOR_H
#define ROOMBUILDER_ROOMDIRECTOR_H
#include "RoomBuilder.h"

class RoomDirector {
RoomBuilder* m_builder;
public:
    RoomDirector(RoomBuilder* builder);
    void setRoomBuilder(RoomBuilder* builder);
    Room* createRoom();
};


#endif //ROOMBUILDER_ROOMDIRECTOR_H
