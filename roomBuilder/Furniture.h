//
// Created by moonchel on 26.11.2020.
//

#ifndef SPACEADVENTURES_FURNITURE_H
#define SPACEADVENTURES_FURNITURE_H
#include <vector>
#include "weapon.h"
#include "Food.h"
#include <iostream>
class Furniture {
    std::string m_fname;
    std::vector<Weapon*> m_fweapons;
    std::vector<Food*> m_ffood;
public:
    Furniture(std::string fname);
    void addItem(Weapon* w);
    void addItem(Food* d);
    //void printItems();
    std::string getfName();
};


#endif //SPACEADVENTURES_FURNITURE_H
