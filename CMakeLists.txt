cmake_minimum_required(VERSION 3.17)
project(spaceadventures)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_C_COMPILER C:/MinGW/bin/gcc)
set(CMAKE_CXX_COMPILER C:/MinGW/bin/g++)

add_executable(spaceadventures
        roomBuilder
        roomBuilder/FirstRoom.cpp
        roomBuilder/FirstRoom.h
        roomBuilder/Food.cpp
        roomBuilder/Food.h
        roomBuilder/Furniture.cpp
        roomBuilder/Furniture.h
        roomBuilder/Room.cpp
        roomBuilder/Room.h
        roomBuilder/RoomBuilder.cpp
        roomBuilder/RoomBuilder.h
        roomBuilder/RoomDirector.cpp
        roomBuilder/RoomDirector.h
        roomBuilder/weapon.cpp
        roomBuilder/weapon.h
        TextState/First.cpp
        TextState/First.h
        TextState/TextState.cpp
        TextState/TextState.h
        main.cpp Ajzek.cpp Ajzek.h main.cpp Monsters.cpp Monsters.h Game.cpp Game.h roomBuilder/SecondRoom.cpp roomBuilder/SecondRoom.h TextState/Second.cpp TextState/Second.h roomBuilder/RoomSwitcher.cpp roomBuilder/RoomSwitcher.h)

