//
// Created by Asus on 18.11.2020.
//

#include "Ajzek.h"
Ajzek* Ajzek::m_instance = nullptr;

Ajzek::Ajzek(int hp,std::string name,int attack){
    m_name = name;
    m_hp = hp;
    m_attack = attack;
    m_currentWeapon= nullptr;
    m_inventoryGuns = {};
    m_inventoryFood ={};
    //m_roomsList={};
}
Ajzek* Ajzek::getInstance() {
    if(m_instance == nullptr){
        std::string Name;
        cout<<"Enter your name, Human Creature!\n My name is: \n";
        while(Name==""){
            std::cin >> Name;

        }
        m_instance = new Ajzek(100, Name, 20);
        return m_instance;
    }

}
void Ajzek::takeItem(Weapon* w){
    cout<<w->getName()<<" is picked\n";
    m_inventoryGuns.push_back(w);
}
void Ajzek::takeItem(Food* f){
    cout<<f->getName()<<" is picked\n";
    m_inventoryFood.push_back(f);
}
float Ajzek::getAttack() {
    if (m_currentWeapon != nullptr){
        return m_attack;
    }
}
float Ajzek::getHP() {
    return m_hp;
}

