//
// Created by moonchel on 26.11.2020.
//

#ifndef SPACEADVENTURES_GAME_H
#define SPACEADVENTURES_GAME_H
#include "Ajzek.h"
#include <iostream>
#include "roomBuilder/RoomDirector.h"
#include "roomBuilder/FirstRoom.h"
#include "roomBuilder/RoomSwitcher.h"

class Game{

    Ajzek* ajzek;
    RoomDirector* dir;
    Room* first;
    Room* second;
    RoomSwitcher* roomSwitcher;
public:
    void game();


};

#endif //SPACEADVENTURES_GAME_H
