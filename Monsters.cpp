//
// Created by moonchel on 18.11.2020.
//

#include "Monsters.h"


Monsters::Monsters(int hp, int attack, std::string name){
    m_name= name;
    m_hp = hp;
    m_attack = attack;
};
float Monsters::getHP() {
    return m_hp;
}
float Monsters::getAttack() {
    return m_attack;
}
void Monsters::setHP(float amount) {
    m_hp+=amount;
    std::cout<<"Monst HP:"<<m_hp<<std::endl;
}
std::string Monsters::getName(){
    return m_name;
};

