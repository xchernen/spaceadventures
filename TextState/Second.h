//
// Created by moonchel on 14.12.2020.
//

#ifndef SPACEADVENTURES_SECOND_H
#define SPACEADVENTURES_SECOND_H
#include "TextState.h"

class Second: public TextState {
public:
    void printText();
};


#endif //SPACEADVENTURES_SECOND_H
