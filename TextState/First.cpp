//
// Created by moonchel on 09.12.2020.
//

#include "First.h"

void First::printText() {
    string input;
    cout << "...(Press any key to continue)" << endl;
    cin >> input;
    cout << "You are waking up in spaceship Magion,"
            " you have a feeling\n that you haven't slept for years..."
            "(Press any button to continue)\n";
    cin >> input;
    cout << "An attempt to make some move brings to nothing,\n body refuses to answer..."
            "(Press any key to continue)\n";
    cin >> input;
    cout << "From other side of this room you hear a growling, "
            "\na drops of blood are falling from high...(Press any key to continue)\n";
    cin >> input;
    cout << "It feels that it's not alone here... \nIt approaching to you!\n"
            " You must find a weapon to kill it!";
}