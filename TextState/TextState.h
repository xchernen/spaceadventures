//
// Created by moonchel on 09.12.2020.
//

#ifndef ROOMBUILDER_TEXTSTATE_H
#define ROOMBUILDER_TEXTSTATE_H
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::string;

class TextState {
public:
    virtual void printText() = 0;
};


#endif //ROOMBUILDER_TEXTSTATE_H
