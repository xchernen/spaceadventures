//
// Created by moonchel on 09.12.2020.
//

#ifndef ROOMBUILDER_FIRST_H
#define ROOMBUILDER_FIRST_H
#include "TextState.h"

class First: public TextState {
public:
    void printText();
};


#endif //ROOMBUILDER_FIRST_H
