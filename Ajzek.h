//
// Created by Asus on 18.11.2020.
//

#ifndef SPACEADVENTURES_AJZEK_H
#define SPACEADVENTURES_AJZEK_H

#include "roomBuilder/Weapon.h"
#include <vector>
#include <iostream>
#include "roomBuilder/Room.h"
#include <typeinfo>
#include "Monsters.h"

class Ajzek{
std::string m_name;
int m_hp;
int m_attack;

 // vectors
Weapon* m_currentWeapon;
std:: vector <Weapon*> m_inventoryGuns;
std::vector <Food*> m_inventoryFood;

    Ajzek(int hp,std::string name,int attack);
    static Ajzek* m_instance;
public:
    static Ajzek* getInstance();
    void setName();
    float getHP();
    float getAttack();
    void takeItem(Weapon* w);
    void takeItem(Food* f);
};


#endif //SPACEADVENTURES_AJZEK_H
