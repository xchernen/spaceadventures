//
// Created by moonchel on 18.11.2020.
//

#ifndef SPACEADVENTURES_MONSTERS_H
#define SPACEADVENTURES_MONSTERS_H



#include <iostream>

class Monsters{
   std::string m_name;
   int m_hp;
   int m_attack;
public:
    Monsters(int hp, int attack, std::string name);
    void setHP(float amount);
    float getHP();
    float getAttack();
    std::string getName();

};


#endif //SPACEADVENTURES_MONSTERS_H
